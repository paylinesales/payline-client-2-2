import React from "react";

const InfoBox: React.FC<{
  className?: string;
  imageSrc: string;
  title: string;
  description: string;
  link: string;
}> = (props) => {
  const {
    imageSrc,
    title,
    description,
    children,
    className = "",
    link,
  } = props;
  return (
    <div
      className={`relative shadow-lg hover:shadow-xl transition-shadow px-4 pt-20 pb-8 xs:p-8 flex flex-wrap border-2 border-solid rounded-lg border-payline-border-light ${className}`}>
      <div className="absolute -top-20 left-0 xs:static w-full md:w-4/12">
        <img src={imageSrc} className="w-full h-36 xs:h-fit-content pr-3" />
      </div>
      <div className="w-full md:w-8/12">
        <h5 className="text-payline-dark text-2xl mb-4 font-bold md:font-medium">
          {title}
        </h5>
        <p className="text-payline-black">
          {description}...&nbsp;
          <a
            href={link}
            className="uppercase text-payline-dark inline font-bold">
            READ MORE
          </a>
        </p>
      </div>
      {children}
    </div>
  );
};

InfoBox.defaultProps = {
  className: "",
};

export default InfoBox;
