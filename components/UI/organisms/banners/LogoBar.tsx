/* eslint-disable @typescript-eslint/no-shadow */
import React from "react";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const LogoBar = ({ logoBar }) => {
  const { logoGroup } = logoBar;
  return (
    <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
      <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
        {logoGroup?.map((logo) => {
          const { sourceUrl } = logo?.logo;
          return <img className="max-w-6rem" src={sourceUrl} />;
        })}
      </div>
    </div>
  );
};

export default LogoBar;
