import React from "react";
import Image from "next/image";
import { PAYMENT_TYPES } from "../../../../constants";

const { ONLINE, IN_PERSON } = PAYMENT_TYPES;

type paymentOptionsBannerProps = {
  className: string;
  businessType: string;
  businessTypeHandler: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

const PaymentOptionsBanner: React.FC<paymentOptionsBannerProps> = ({
  className: classes,
  children,
  businessType,
  businessTypeHandler,
}) => {
  return (
    <div className={classes}>
      {children}
      <div className="flex flex-wrap justify-center lg:justify-start">
        <button
          type="button"
          data-type={ONLINE}
          onClick={businessTypeHandler}
          className={`w-1/2 flex justify-center items-center gap-3 text-lg lg:text-3xl xl:text-4xl font-bold focus:text-payline-black ${
            businessType === ONLINE
              ? "text-payline-black"
              : "text-payline-disabled"
          }`}>
          <Image src="/images/svg/online.svg" width={30} height={30} />
          <h4>Online</h4>
        </button>
        <button
          type="button"
          data-type={IN_PERSON}
          onClick={businessTypeHandler}
          className={`w-1/2 flex justify-center items-center gap-3 text-lg lg:text-3xl xl:text-4xl font-bold focus:text-payline-black ${
            businessType === IN_PERSON
              ? "text-payline-black"
              : "text-payline-disabled"
          }`}>
          <Image src="/images/svg/in-person.svg" width={30} height={30} />
          <h4>In-Person</h4>
        </button>
      </div>
    </div>
  );
};

export default PaymentOptionsBanner;
