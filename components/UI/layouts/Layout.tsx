import React, { useEffect, useRef } from "react";
import Head from "next/head";
import Navbar from "@/components/UI/organisms/Navbar/Navbar";
// import TagManager from "react-gtm-module";
import CTABanner from "../organisms/cards/CTABanner";
import Footer from "../templates/footer/footer";

type layoutProps = {
  children: React.ReactNode;
  navBar: any;
};

// const tagManagerArgs = {
//   gtmId: "GTM-W9W8RPJ",
// };
// TagManager.initialize(tagManagerArgs);

const Layout: React.FC<layoutProps> = ({ children, navBar }) => {
  const pageHeaderRef = useRef<HTMLDivElement | null>(null);
  useEffect(() => {
    const onScroll = () => {
      const { scrollY } = window;
      if (pageHeaderRef.current) {
        if (scrollY > 30) {
          pageHeaderRef.current.style.backgroundColor = "white";
        } else {
          pageHeaderRef.current.style.backgroundColor = "transparent";
        }
      }
    };

    window.addEventListener("scroll", onScroll);
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  // TODO: fetch this data from the api
  const bannerData = { lead: "Get the first", bold: "2 months", end: "on us!" };

  return (
    <div className="relative">
      <Head>
        <title>My page title</title>
        <meta property="og:title" content="My page title" key="title" />
      </Head>
      <div className="w-screen bg-transparent fixed z-10 " ref={pageHeaderRef}>
        <CTABanner {...bannerData} />
        <Navbar navBar={navBar} />
      </div>
      <div id="content" className="pt-12">
        {children}
      </div>
      <Footer />
    </div>
  );
};

export default Layout;
