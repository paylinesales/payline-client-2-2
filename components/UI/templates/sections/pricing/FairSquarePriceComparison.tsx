import React, { ReactElement } from "react";
import FairSquarePrice from "@/components/UI/organisms/Table/PriceTable";

const SectionWrapper = (props): ReactElement => {
  const { classes, children } = props;
  return <div className={classes}>{children}</div>;
};

const FairSquarePriceComparison = (): ReactElement => {
  const testData = [
    {
      title: "",
      data: [
        "Monthly fee",
        "Annual Fee",
        "Per Transaction Fee",
        "Swiped Debit Card",
        "Keyed Debit Card",
        "Swiped Credit Card",
        "Keyed Credit Card",
        "Virtual Terminal",
        "Cancellation Fee",
        "Deposit Time",
      ],
    },
    {
      title: "PAYLINE SWIPE",
      data: [
        "$10.00",
        "$0.00",
        "10¢",
        "0.25%",
        "0.25%",
        "1.71%",
        "1.90%",
        "$10.00",
        "$0.00",
        "1 - 2 days",
      ],
    },
    {
      title: "PAYLINE KEYED-IN",
      data: [
        "$10.00",
        "$0.00",
        "20¢",
        "0.35%",
        "0.35%",
        "1.81%",
        "2.00%",
        "$10.00",
        "$0.00",
        "1 - 2 days",
      ],
    },
    {
      title: "SQUARE",
      data: [
        "$0.00",
        "$0.00",
        "$0.00",
        "2.75%",
        "3.50%",
        "2.75%",
        "3.50%",
        "$0.00",
        "$0.00",
        "1 -2 days",
      ],
    },
    {
      title: "PAYPAL",
      data: [
        "$0.00",
        "$0.00",
        "$0.00",
        "2.70%",
        "3.50%",
        "2.70%",
        "3.50%",
        "$0.00",
        "$0.00",
        "3 -5 days",
      ],
    },
    {
      title: "BANK",
      data: [
        "$20.00",
        "$75.00",
        "10¢",
        "1.20%",
        "3.20%",
        "1.75%",
        "3.20%",
        "$20.00",
        "$250.00",
        "2-5+ days",
      ],
    },
  ];

  return (
    <SectionWrapper classes="flex justify-center w-100 hidden md:block">
      <div className="py-16 md:py-6">
        <div className="my-6 flex flex-row justify-center">
          TRANSPARENT PRICING
        </div>
        <div className="my-6 text-center justify-center text-4xl font-hkgrotesk leading-tight font-bold text-payline-black">
          A fair and Square
          <span className="bg-payline-blue text-payline-white mr-4 px-2">
            price comparison
          </span>
        </div>
        <div className="my-6">
          <FairSquarePrice data={testData} />
        </div>
      </div>
    </SectionWrapper>
  );
};

export default FairSquarePriceComparison;
