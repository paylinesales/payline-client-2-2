import React from "react";

import SectionTitle from "components/UI/atoms/SectionTitle";
import PaylineDetail from "components/UI/atoms/PaylineDetail";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { WebsiteCopy_Faqsection as FaqType } from "@/generated/apolloComponents";
import { PAYMENT_TYPES, ICON_POSITION } from "../../../../../constants";

const { ONLINE } = PAYMENT_TYPES;
const { RIGHT } = ICON_POSITION;

interface FaqProps {
  faqData: FaqType;
}

const FAQSection: React.FC<FaqProps> = ({ faqData }) => {
  const details = faqData.faqs;
  const { paymentType } = useBusinessCategory();

  // for as long as the payment type is on line then the business is online. Variable left for clarity but we would have use this directly
  const isOnlineBusiness = paymentType === ONLINE;

  const sectionClass = isOnlineBusiness ? "bg-faq-online" : "bg-faq-in-person";
  return (
    <section
      id="faq-section"
      className={`${sectionClass} bg-cover bg-no-repeat pt-24 pb-32 lg:pb-24 px-8 md:px-16 lg:px-20 xl:px-32`}>
      <div className="container mx-auto">
        <SectionTitle
          lastPartOfHeadline="Asked Questions"
          highlighedText="Frequently"
        />
        <div className=" w-full lg:w-8/12 mx-auto mt-14">
          {details.map((detail) => (
            <PaylineDetail
              title={detail.question}
              className="rounded bg-white hw-details transition-colors duration-150"
              summaryClasses="text-lg font-semibold"
              iconPosition={RIGHT}
              icon={faPlus}
              iconWhenOpened={faMinus}>
              <p>{detail.answer}</p>
            </PaylineDetail>
          ))}
        </div>
      </div>
    </section>
  );
};

export default FAQSection;
